package clientservice

import (
	"log"
	"mimo/configs"
	"mimo/helpers"
	"mimo/models"
	"net/http"
	"strings"
	"time"

	"github.com/labstack/echo"
)

func (svc nHierarchyClientServices) UpdateClientService(ctx echo.Context) error {
	var (
		svcName  = "UpdateClientService"
		logErr   = "Error " + svcName
		req      models.ReqGetListClient
		response models.ResGetNClient
		dbTime   = time.Now().Format(time.RFC3339)
	)
	_, err := helpers.BindValidate(&req, ctx)
	if err != nil {
		log.Println(logErr+"BINDING", err.Error())
		result := helpers.ResponseJSON(configs.FALSE_VALUE, configs.VALIDATE_ERROR_CODE, "FAILLED "+err.Error(), nil)
		return ctx.JSON(http.StatusOK, result)
	}
	if req.ID == 0 {
		log.Println(logErr + " ID cannot be empty")
		result := helpers.ResponseJSON(configs.FALSE_VALUE, configs.VALIDATE_ERROR_CODE, "ID cannot be empty ", nil)
		return ctx.JSON(http.StatusOK, result)
	}
	if req.ClientName == "" {
		log.Println(logErr + " Client Name cannot be empty")
		result := helpers.ResponseJSON(configs.FALSE_VALUE, configs.VALIDATE_ERROR_CODE, "Client Name cannot be empty ", nil)
		return ctx.JSON(http.StatusOK, result)
	}
	req.ClientName = strings.ToUpper(req.ClientName)

	response, err = svc.services.ApiNHierarchy.NUpdateClient(models.ReqUpdateNClient{
		ID:         req.ID,
		ClientName: req.ClientName,
		UpdatedAt:  dbTime,
		UpdatedBy:  "sys",
	})
	if err != nil {
		log.Println(logErr+"NCreateClient", err.Error())
		result := helpers.ResponseJSON(configs.FALSE_VALUE, configs.DB_ERROR, "FAILLED "+err.Error(), nil)
		return ctx.JSON(http.StatusOK, result)
	}
	result := helpers.ResponseJSON(configs.TRUE_VALUE, configs.SUCCESS_CODE, "Success", response)
	return ctx.JSON(http.StatusOK, result)
}
