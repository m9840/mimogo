package configs

const (
	MONGO_DB_DEV            = "dbDevRetailAuth"
	APP_VERSION             = "1.0.0-SNAPSHOT"
	REGISTRY_URL            = "119.2.53.151:5000"
	EMPTY_VALUE_INT         = 0
	EMPTY_VALUE             = ""
	TRUE_VALUE              = true
	FALSE_VALUE             = false
	MONGO_COL_LOG_LOGIN     = "colDataLogin"
	MONGO_COL_LOGIN_SESSION = "colDataSesion"

	VALIDATE_ERROR_CODE    = "34"
	SUCCESS_CODE           = "00"
	DB_ERROR               = "81"
	DB_NOT_FOUND           = "82"
	LAYOUT_TIMESTAMP       = "2006-01-02 15:04:05"
	LAYOUT_TIMESTAMPTRX    = "20060102"
	MONGO_COL_RESEND_EMAIL = "colVerificationEmailAttempts"
	KEY_MIMO               = "MKPRetailDev2022"
	// KEY_MIMO               = "MIMOGOALSFOR2024"
)
