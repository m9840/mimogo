package trxrepo

import "mimo/repositories"

type trxRepository struct {
	repo repositories.Repositories
}

func NewTrxRepo(repo repositories.Repositories) trxRepository {
	return trxRepository{
		repo: repo,
	}
}
